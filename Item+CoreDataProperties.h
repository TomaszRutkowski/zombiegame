//
//  Item+CoreDataProperties.h
//  ZombieGame
//
//  Created by TheAppExperts on 3/10/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import "Item+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Item (CoreDataProperties)

+ (NSFetchRequest<Item *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSString *type;
@property (nonatomic) int16_t qty;
@property (nonatomic) int16_t ammo;
@property (nullable, nonatomic, retain) Account *owner;

@end

NS_ASSUME_NONNULL_END
