//
//  Account+CoreDataClass.h
//  ZombieGame
//
//  Created by TheAppExperts on 3/10/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Item, Stats;

NS_ASSUME_NONNULL_BEGIN

@interface Account : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Account+CoreDataProperties.h"
