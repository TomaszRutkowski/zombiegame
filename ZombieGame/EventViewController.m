//
//  EventViewController.m
//  ZombieGame
//
//  Created by TheAppExperts on 3/14/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import "EventViewController.h"

@interface EventViewController ()

@end

@implementation EventViewController

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([self.feedback class])
    {
        self.feedback = [[UINotificationFeedbackGenerator alloc] init];
        [self.feedback notificationOccurred: UINotificationFeedbackTypeSuccess];
    }
    [self addRandomItem];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)leaveScreen:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^(void){}];
}

- (void)addRandomItem
{
    NSError *error;
    Item *newItem = [Item RandomItemWithContext:[LoggedInData sharedLoggedInData].LoggedInAccount.managedObjectContext];
    NSMutableOrderedSet *newAccountItems = [[LoggedInData sharedLoggedInData].LoggedInAccount.items mutableCopy];
    
    bool found = FALSE;
    for (Item *item in newAccountItems) {
        if ([item.name isEqualToString: newItem.name])
        {
            item.qty = item.qty + newItem.qty;
            found = TRUE;
        }
    }
    if (found == FALSE)
    {
        [newAccountItems addObject:newItem];
    }
    
    [LoggedInData sharedLoggedInData].LoggedInAccount.items = newAccountItems;
    [[LoggedInData sharedLoggedInData].LoggedInAccount.managedObjectContext save:&error];
    if (error)
    {
        [self.foundLabel setText:@"Error"];
    }
    else
    {
        [self.foundLabel setText: [NSString stringWithFormat:@"Found: %@", newItem.name]];
        [self.itemImage setImage:newItem.imageForItem];
    }
}

@end
