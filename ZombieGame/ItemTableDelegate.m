//
//  ItemTableDelegate.m
//  ZombieGame
//
//  Created by TheAppExperts on 3/10/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import "ItemTableDelegate.h"

@implementation ItemTableDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Items";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[LoggedInData sharedLoggedInData].LoggedInAccount.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *tvc = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Item"];
    [tvc.textLabel setText: [[LoggedInData sharedLoggedInData].LoggedInAccount.items objectAtIndex:indexPath.row].name];
    [tvc.detailTextLabel setText:[[NSNumber numberWithShort:[[LoggedInData sharedLoggedInData].LoggedInAccount.items objectAtIndex:indexPath.row].qty] stringValue]];
    [tvc setBackgroundColor:[UIColor redColor]];
    
    return tvc;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   [self.delegate SegueWithItem:[[LoggedInData sharedLoggedInData].LoggedInAccount.items objectAtIndex:indexPath.row]];
}


@end
