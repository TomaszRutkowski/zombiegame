//
//  MapViewController.h
//  ZombieGame
//
//  Created by TheAppExperts on 3/13/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "Stats+StatsMethods.h"
#import "LoggedInData.h"
#import "MBProgressHUD.h"
#import "SafehouseCacheManager.h"
#import "SafehouseViewController.h"

/*!
* @brief The view controller displaying the map.
* @discussion This view controller contains the map code due to it being redundant to create a new class for the core piece of the game that will only ever be in one location. The map first retrieves information about safehouses and the player's location, asking for any necessary authorisation. The player is then rendered onto the world, where walking a certain distance (100m) triggers the chance of a random event. This is one tab of three, and the tabs can be switched between at any time.
*/

@interface MapViewController : UIViewController <CLLocationManagerDelegate, MKMapViewDelegate, SafehouseDelegate>

/*!
 * @brief The location manager responsible for the player's position tracking.
 */
@property (strong, nonatomic) CLLocationManager *locationManager;

/*!
 * @brief The manager for retrieving safehouse information to be used.
 */
@property (strong, nonatomic) SafehouseCacheManager *safehouseManager;

/*!
 * @brief The view containing the map to be displayed.
 */
@property (strong, nonatomic) IBOutlet MKMapView *mapView;

/*!
 * @brief A convenience view for displaying when information is loading.
 */
@property (strong, nonatomic) MBProgressHUD *loading;

/*!
 * @brief An array holding all safehouse information once loaded by the manager.
 */
@property (strong, nonatomic) NSArray *arrayOfSafehouses;

/*!
 * @brief A variable storing the name of a selected safehouse, if there is one.
 */
@property (strong, nonatomic) NSString *selected;

/*!
 * @brief The action when the user taps.
 * @param sender Unused.
 */
- (IBAction)userTapped:(id)sender;

/*!
 * @brief This method triggers a chance of a random event.
 * @return Whether an event actually triggered or not.
 */
- (BOOL)randomEvent;

@end
