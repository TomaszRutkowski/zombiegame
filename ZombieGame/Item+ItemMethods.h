//
//  Item+ItemMethods.h
//  ZombieGame
//
//  Created by TheAppExperts on 3/10/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import "Item+CoreDataClass.h"
#import "Stats+StatsMethods.h"
#import "Account+AccountMethods.h"
#import <UIKit/UIKit.h>

@interface Item (ItemMethods)

+(Item *)StarterItemWithContext:(NSManagedObjectContext *)context;
+(Item *)RandomItemWithContext:(NSManagedObjectContext *)context;
-(UIImage *)imageForItem;
- (void)useItemInAccount:(Account *)account;

@end
