//
//  RegisterViewController.m
//  ZombieGame
//
//  Created by TheAppExperts on 3/6/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import "RegisterViewController.h"

@interface RegisterViewController ()
{
    MFMailComposeViewController *mailController;
}

@end

@implementation RegisterViewController

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.usernameField resignFirstResponder];
    [self.passwordField resignFirstResponder];
    [self.emailField resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return TRUE;
}

- (void)mailFinished:(BOOL)Sent
{
    //sent parameter unused atm, but could be used for future logic
    [self dismissViewControllerAnimated:YES completion:^(void){}];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.mailDelegate = [[MailDelegate alloc] init];
    self.usernameField.delegate = self;
    self.passwordField.delegate = self;
    self.emailField.delegate = self;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Account"];
    [request setReturnsObjectsAsFaults:FALSE];
    NSError *error = nil;
    NSArray *results = [[CoreDataManager sharedCoreDataManager].managedObjectContext executeFetchRequest:request error:&error];
    if ([results count] == 0)
    {
        NSLog(@"Data is Empty");
    }
    else
    {
        NSLog(@"%@", [results objectAtIndex:0]);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)goBack:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)yesActionwithName:(NSString *)name Password:(NSString *)password Email:(NSString *)email;
{
    mailController = [[MFMailComposeViewController alloc] init];
    if ([MFMailComposeViewController canSendMail])
    {
        mailController.mailComposeDelegate = self.mailDelegate;
        [mailController setSubject:@"Zombie Game Reminder"];
        [mailController setMessageBody:[NSString stringWithFormat:@"Name: %@ \n Password: %@",name,password] isHTML:NO];
        [mailController setToRecipients:[NSArray arrayWithObject:email]];

        dispatch_async(dispatch_get_main_queue(),
        ^{
            [self presentViewController:mailController animated:YES completion:^(void){}];
        });
    }
    else
    {
        UIAlertController *alert = [[UIAlertController alloc] init];
        [alert setTitle:@"No Mail Account"];
        [alert setMessage:@"Your account was created successfully, but you have not configured email, so we cannot send you a reminder message."];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){[self dismissViewControllerAnimated:YES completion:^(void){}];}];
        [alert addAction:ok];
        dispatch_async(dispatch_get_main_queue(),
        ^{
            [self presentViewController:alert animated:YES completion:^(void){}];
        });
    }
}

- (void)noAction
{
    dispatch_async(dispatch_get_main_queue(),
    ^{
        [self dismissViewControllerAnimated:YES completion:^{}];
    });
}

- (IBAction)registerAction:(id)sender
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
    ^{
        NSString *name = self.usernameField.text;
        NSString *password = self.passwordField.text;
        NSString *email = self.emailField.text;
        
        if ([Account validName:name] && [Account validPassword:password] && [Account validEmail:email])
        {
            NSManagedObjectContext *context = [CoreDataManager sharedCoreDataManager].managedObjectContext;
            if ([Account createAccount:name withPass:password withEmail:email withContext:context])
            {
                UIAlertController *alert = [[UIAlertController alloc] init];
                [alert setTitle:@"Account Created"];
                [alert setMessage:@"Your account was created successfully, would you like to send an email to remind you of the details?"];
                UIAlertAction *yes = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){[self yesActionwithName:name Password:password Email:email];}];
                UIAlertAction *no = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){[self noAction];}];
                [alert addAction:yes];
                [alert addAction:no];
                dispatch_async(dispatch_get_main_queue(),
                ^{
                    [self presentViewController:alert animated:YES completion:^(void){}];
                });
            }
            else
            {
                UIAlertController *alert = [[UIAlertController alloc] init];
                [alert setTitle:@"Account Failure"];
                [alert setMessage:@"Your account failed to save. The account name may already be taken or you may have insufficient disk space available."];
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){}];
                [alert addAction:ok];
                dispatch_async(dispatch_get_main_queue(),
                ^{
                    [self presentViewController:alert animated:YES completion:^(void){}];
                });
            }
        }
        else
        {
            UIAlertController *alert = [[UIAlertController alloc] init];
            [alert setTitle:@"Registration Error"];
            [alert setMessage:@"Your name, password or email is invalid. Names and passwords can only use alphanumeric characters, and the email must be a valid email address."];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){}];
            [alert addAction:ok];
            dispatch_async(dispatch_get_main_queue(),
            ^{
                [self presentViewController:alert animated:YES completion:^(void){}];
            });
        }
    });
}

- (BOOL)prefersStatusBarHidden
{
    return TRUE;
}

@end
