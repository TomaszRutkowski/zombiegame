//
//  MenuTableViewController.m
//  ZombieGame
//
//  Created by TheAppExperts on 3/7/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import "MenuTableViewController.h"

@interface MenuTableViewController ()

@end

@implementation MenuTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView setBackgroundColor:[UIColor colorWithRed:0 green:0.5 blue:0 alpha:1]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Item"];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Item"];
        cell.backgroundColor = [UIColor colorWithRed:0 green:0.5 blue:0 alpha:1];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    switch (indexPath.row) {
        case 0:
            [cell.textLabel setText:@"Settings"];
            break;
            
        case 1:
            [cell.textLabel setText:@"Tutorial"];
            break;
            
        case 2:
            [cell.textLabel setText:@"Logout"];
            break;
            
        default:
            [cell.textLabel setText:@"Error"];
            break;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return tableView.frame.size.height/4;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row)
    {
        case 0:
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]]; //ios 9 compatible
            [self.tableView deselectRowAtIndexPath: indexPath animated:NO];
            break;
        }
        case 1:
        {
           TutorialViewController *viewToPush = [[UIStoryboard storyboardWithName:@"Main" bundle: [NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"Tutorial"];
            [self.navigationController pushViewController:viewToPush animated:YES];
            break;
        }
        case 2:
        {
            [self logout];
            break;
        }
        default:
            NSLog(@"Error; no functionality defined!");
            break;
    }
}


- (void)logout
{
    //get the tab bar, and dismiss it, to return to the main screen
    [[[UIApplication sharedApplication] keyWindow].rootViewController dismissViewControllerAnimated:YES completion:^{
    }];
}

@end
