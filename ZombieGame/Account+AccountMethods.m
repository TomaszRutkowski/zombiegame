//
//  Account+AccountMethods.m
//  ZombieGame
//
//  Created by TheAppExperts on 3/9/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import "Account+AccountMethods.h"

@implementation Account (AccountMethods)

+ (BOOL) validName:(NSString *)name
{
    NSString *nameRegex = @"^[A-Z0-9a-z]+$";
    NSPredicate *nameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", nameRegex];
    return [nameTest evaluateWithObject:name];
}

+ (BOOL) validPassword:(NSString *)password
{
    NSString *passRegex = @"^[A-Z0-9a-z]+$";
    NSPredicate *passTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", passRegex];
    return [passTest evaluateWithObject:password];
}

+ (BOOL) validEmail:(NSString *)email
{
    NSString *emailRegex = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

+(Account *)createAccount:(NSString *) name withPass:(NSString *) password withEmail:(NSString *) email withContext:(NSManagedObjectContext *) context{

    Account *newAccount = [[Account alloc] initWithEntity:[NSEntityDescription entityForName:@"Account" inManagedObjectContext:context]insertIntoManagedObjectContext:context];
    @try
    {
        NSError *error;
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Account"];
        [request setPredicate:[NSPredicate predicateWithFormat:@"name == %@", name]];
        NSArray *results = [context executeFetchRequest:request error:&error];
        if (!results)
        {
            NSLog(@"Failed to get accounts");
            return nil;
        }
        else
        {
            if ([results count] > 0)
            {
                NSLog(@"Account already exists");
                return nil;
            }
        }
        [newAccount setValue:name forKey:@"name"];
        [newAccount setValue:password forKey:@"password"];
        [newAccount setValue:email forKey:@"email"];
        if(![context save:&error])
        {
            NSLog(@"Failed to save");
            return nil;
        }
    }
    @catch (NSException *e)
    {
        NSLog(@"%@", e.description);
        return nil;
    }
    return newAccount;
}

+(Account *)loginAccount:(NSString *)name withPass:(NSString *)password withContext:(NSManagedObjectContext *) context
{
    NSArray *arrayOfPredicates = @[[NSPredicate predicateWithFormat:@"name == %@", name], [NSPredicate predicateWithFormat:@"password == %@", password]];
    NSPredicate *allPredicates = [NSCompoundPredicate andPredicateWithSubpredicates:arrayOfPredicates];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Account"];
    [request setPredicate:allPredicates];
    NSError *error;
    NSArray *results = [context executeFetchRequest:request error:&error];
    if (!results)
    {
        NSLog(@"Failed to get accounts");
        return nil;
    }
    else
    {
        if ([results count] > 0)
        {
            return (Account *)[results firstObject];
        }
        else
        {
            NSLog(@"Account not found");
            return nil;
        }
    }
}

@end
