//
//  CombatViewController.m
//  ZombieGame
//
//  Created by TheAppExperts on 3/14/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import "CombatViewController.h"

@interface CombatViewController ()

@end

@implementation CombatViewController

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.playerHealthBar.progress = [LoggedInData sharedLoggedInData].LoggedInAccount.stats.health/100;
    self.enemyHealthMax = 100;
    self.enemyHealth = self.enemyHealthMax;
    self.enemyHealthBar.progress = self.enemyHealth/self.enemyHealthMax;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)win
{
    NSError *error;
    [self dismissViewControllerAnimated:YES completion:^{}];
    [[LoggedInData sharedLoggedInData].LoggedInAccount.managedObjectContext save:&error]; //doesn't matter if it doesn't work
}

-(void)death
{
    [self performSegueWithIdentifier:@"CombatToDeath" sender:self];
}

-(void)weaponDamage
{
    self.enemyHealth = self.enemyHealth - 40; //replace in future
}

-(void)enemyDamage
{
    [LoggedInData sharedLoggedInData].LoggedInAccount.stats.health = [LoggedInData sharedLoggedInData].LoggedInAccount.stats.health - 12; //replace in future
    if ([self.feedback class])
    {
        self.feedback = [[UINotificationFeedbackGenerator alloc] init];
        [self.feedback notificationOccurred: UINotificationFeedbackTypeSuccess];
    }
}

- (IBAction)userSwiped:(id)sender {
    [self weaponDamage];
    self.enemyHealthBar.progress = self.enemyHealth/self.enemyHealthMax;
    if (self.enemyHealth <= 0)
    {
        [self win];
    }
    else
    {
        [self enemyDamage];
        self.playerHealthBar.progress = [LoggedInData sharedLoggedInData].LoggedInAccount.stats.health/100;
        if ([LoggedInData sharedLoggedInData].LoggedInAccount.stats.health <= 0)
        {
            [self death];
        }
    }
}

@end
