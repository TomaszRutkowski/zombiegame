//
//  InventoryViewController.m
//  ZombieGame
//
//  Created by TheAppExperts on 3/8/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import "InventoryViewController.h"

@interface InventoryViewController ()

@property (strong, nonatomic) ItemTableDelegate *itd;

@end

@implementation InventoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.itd = [[ItemTableDelegate alloc] init];
    [self.itd setDelegate:self];
    [self.inventoryTableView setDelegate:self.itd];
    [self.inventoryTableView setDataSource:self.itd];
    [self.inventoryTableView setTableFooterView:[UIView new]];
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"colorblind"] boolValue] == TRUE)
    {
        self.hungerBar.progressTintColor = [UIColor grayColor];
        self.tirednessBar.progressTintColor = [UIColor whiteColor];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    float healthFloat = (float)[LoggedInData sharedLoggedInData].LoggedInAccount.stats.health;
    [self.healthBar setProgress: healthFloat/100];
    float hungerFloat = (float)[LoggedInData sharedLoggedInData].LoggedInAccount.stats.hunger;
    [self.hungerBar setProgress: hungerFloat/100];
    float thirstFloat = (float)[LoggedInData sharedLoggedInData].LoggedInAccount.stats.thirst;
    [self.thirstBar setProgress: thirstFloat/100];
    float tirednessFloat = (float)[LoggedInData sharedLoggedInData].LoggedInAccount.stats.tiredness;
    [self.tirednessBar setProgress: tirednessFloat/100];
    [self.inventoryTableView reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowItem"])
    {
        ((ItemViewController *)segue.destinationViewController).lookedAtItem = sender;
    }
}

-(void)SegueWithItem:(Item *)item
{
    [self performSegueWithIdentifier:@"ShowItem" sender:item];
}

@end
