//
//  Stats+StatsMethods.m
//  ZombieGame
//
//  Created by TheAppExperts on 3/9/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import "Stats+StatsMethods.h"

@implementation Stats (StatsMethods)

+ (Stats *)initStatsWithContext:(NSManagedObjectContext*)context
{
    Stats *newStats = [[Stats alloc] initWithEntity: [NSEntityDescription entityForName:@"Stats" inManagedObjectContext:context] insertIntoManagedObjectContext:context];
    newStats.health = 100;
    newStats.hunger = 100;
    newStats.thirst = 100;
    newStats.tiredness = 100;
    return newStats;
}

- (void) walkingDrainWithNight:(BOOL)night
{
    self.health = self.health; //health only changes in combat
    self.hunger = self.hunger - MAX(((101 - self.hunger)/40),1); //hunger depletes faster the hungrier you are.
    self.thirst = self.thirst - 2; //thirst depletes at a regular rate
    if (night == true) //tiredness depletes faster at night
    {
        self.tiredness = self.tiredness - 3;
    }
    else
    {
        self.tiredness = self.tiredness - 0.5;
    }
    NSError *error;
    [self.managedObjectContext save:&error]; //not too dangerous if it fails to save, game can keep running
}

- (void) changeStatswithItemType:(NSString *)type inAccount:(Account *)account
{
    NSError *error;
    if ([type  isEqual: @"Healing"])
    {
        self.health = 100;
    }
    
    if ([type  isEqual: @"Food"])
    {
        self.hunger = self.hunger + 20;
        if (self.hunger > 100)
        {
            self.hunger = 100;
        }
            
        self.health = self.health + 20;
        if (self.health > 100)
        {
            self.health = 100;
        }
    }
    
    if ([type  isEqual: @"Water"])
    {
        self.thirst = self.thirst + 40;
        if (self.thirst > 100)
        {
            self.thirst = 100;
        }
        
        self.health = self.health + 20;
        if (self.health > 100)
        {
            self.health = 100;
        }
    }
    
    if ([type  isEqual: @"Drug"])
    {
        self.tiredness = 100;
        self.hunger = self.hunger - 10;
        if (self.hunger < 5)
        {
            self.hunger = 5;
        }
    }
    [account.managedObjectContext save:&error];
}

@end
