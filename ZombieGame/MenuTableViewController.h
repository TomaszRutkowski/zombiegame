//
//  MenuTableViewController.h
//  ZombieGame
//
//  Created by TheAppExperts on 3/7/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TutorialViewController.h"

/**
 * @brief A table view controller that handles the main menu.
 */

@interface MenuTableViewController : UITableViewController

- (void)logout;

@end
