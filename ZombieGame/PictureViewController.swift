//
//  PictureViewController.swift
//  ZombieGame
//
//  Created by TheAppExperts on 3/20/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

import UIKit

/**
The view controller for a picture associated with a safehouse in the Zombie Game.
*/

class PictureViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    /**
     The image view associated with the safehouse. It is empty if not found.
     */
    @IBOutlet var safehouseImage: UIImageView!;
    
    /**
     The name of the safehouse to use to fetch the image from memory. (*Optional*)
     */
    var safehouseIn: String?;
    
    /**
     The controller to use the camera.
     */
    let takePictureController = UIImagePickerController();

    /**
     Called to initialise the view after loading.
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        var paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true);
        let documentsPath = paths[0];
        let safehouseName = self.safehouseIn ?? "safehouse";
        let filePath = documentsPath + "/" + safehouseName + ".png";
        do
        {
            try self.safehouseImage.image = UIImage(data: Data(contentsOf: URL(fileURLWithPath: filePath)));
        }
        catch
        {
            NSLog("Could not read file");
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
     Delegate method called after selecting an image from the camera.
     
        - parameters:
            - picker: The camera controller that snapped the image.
            - info: Dictionary referencing the captured image.
     */
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        self.safehouseImage.image = info[UIImagePickerControllerOriginalImage] as! UIImage?;
        DispatchQueue.global(qos: .default).async
        {
            let jpegData = UIImageJPEGRepresentation(self.safehouseImage.image!, 1.0);
            var paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true);
            let documentsPath = paths[0];
            let safehouseName = self.safehouseIn ?? "safehouse";
            let filePath = documentsPath + "/" + safehouseName + ".png";
            do
            {
                try jpegData?.write(to: URL(fileURLWithPath:filePath));
            }
            catch
            {
                NSLog(error.localizedDescription);
                NSLog("Cannot write to file");
            }
        }
            
        takePictureController.dismiss(animated: true, completion: nil);
    }
    
    /**
    Delegate method called after failing to select an image from the camera.
        - parameters:
            -picker: The camera controller that failed to snap the image.
     
    */
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        takePictureController.dismiss(animated: true, completion: nil);
    }
    
    /**
    Method called when the back button is pressed.
    */
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil);
    }
    
    /**
     Method called when the take picture button is pressed.
     */
    
    @IBAction func takePictureAction(_ sender: Any) {
        if (UIImagePickerController.isSourceTypeAvailable(.camera) == true)
        {
            takePictureController.sourceType = .camera;
            takePictureController.cameraDevice = .rear;
            takePictureController.delegate = self;
            self.present(takePictureController, animated: true, completion: nil);
        }
        else
        {
            let alert = UIAlertController(title: "Camera Error", message: "Could not find a usable camera on the device.", preferredStyle: UIAlertControllerStyle.alert);
            let alertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
            alert.addAction(alertAction);
            self.present(alert, animated: true, completion: nil);
        }
    }
    
    
}
