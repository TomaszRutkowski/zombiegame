//
//  InventoryViewController.h
//  ZombieGame
//
//  Created by TheAppExperts on 3/8/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Account+AccountMethods.h"
#import "Stats+StatsMethods.h"
#import "LoggedInData.h"
#import "ItemViewController.h"
#import "ItemTableDelegate.h"

/**
 * @brief The view controller that handles the stat screen and inventory.
 */

@interface InventoryViewController : UIViewController <ItemTableDelegateProtocol>

@property (strong, nonatomic) IBOutlet UILabel *healthLabel;
@property (strong, nonatomic) IBOutlet UILabel *hungerLabel;
@property (strong, nonatomic) IBOutlet UILabel *thirstLabel;
@property (strong, nonatomic) IBOutlet UILabel *tirednessLabel;
@property (strong, nonatomic) IBOutlet UIProgressView *healthBar;
@property (strong, nonatomic) IBOutlet UIProgressView *hungerBar;
@property (strong, nonatomic) IBOutlet UIProgressView *thirstBar;
@property (strong, nonatomic) IBOutlet UIProgressView *tirednessBar;
@property (strong, nonatomic) IBOutlet UITableView *inventoryTableView;


@end
