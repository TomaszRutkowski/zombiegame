//
//  LoginViewController.m
//  ZombieGame
//
//  Created by TheAppExperts on 3/6/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.usernameField resignFirstResponder];
    [self.passwordField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return TRUE;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.usernameField.delegate = self;
    self.passwordField.delegate = self;
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.accountButton setHidden:FALSE];
    [self.loginButton setHidden:FALSE];
    [self.loadingLabel setHidden:TRUE];
}

- (void)viewDidAppear:(BOOL)animated
{

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (Account*)newAccount:(Account*)account withContext:(NSManagedObjectContext *)context
{
    account.stats = [Stats initStatsWithContext:context];
    NSMutableOrderedSet *newItems = [account.items mutableCopy];
    [newItems addObject:[Item StarterItemWithContext:context]];
    account.items = newItems;
    return account;
}

- (IBAction)login:(id)sender
{
    [self.accountButton setHidden:TRUE];
    [self.loginButton setHidden:TRUE];
    [self.loadingLabel setHidden:FALSE];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
    ^{
        NSManagedObjectContext *context = [CoreDataManager sharedCoreDataManager].managedObjectContext;
        Account *loggingIntoAccount = [Account loginAccount:self.usernameField.text withPass:self.passwordField.text withContext:context];
        if (loggingIntoAccount == nil)
        {
            UIAlertController *alert = [[UIAlertController alloc] init];
            [alert setTitle:@"Login Error"];
            [alert setMessage:@"Your username or password is invalid, please try again!"];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){}];
            [alert addAction:ok];
            dispatch_async(dispatch_get_main_queue(),
            ^{
                [self presentViewController:alert animated:YES completion:^(void){}];
                [self.accountButton setHidden:FALSE];
                [self.loginButton setHidden:FALSE];
                [self.loadingLabel setHidden:TRUE];
            });
        }
        else
        {
            if (loggingIntoAccount.stats.health == 0 || loggingIntoAccount.stats == nil)
            {
                loggingIntoAccount = [self newAccount:loggingIntoAccount withContext:context];
            }
            [LoggedInData sharedLoggedInData].LoggedInAccount = loggingIntoAccount;
            dispatch_async(dispatch_get_main_queue(),
            ^{
                [self performSegueWithIdentifier:@"LoginToMain" sender:sender];
            });
        }
    });
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)prefersStatusBarHidden
{
    return TRUE;
}

@end
