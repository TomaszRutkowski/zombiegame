//
//  DeathViewController.h
//  ZombieGame
//
//  Created by TheAppExperts on 3/15/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import <UIKit/UIKit.h>

/*!
 * @brief The view controller that shows up when you die. Does little except force you to log out.
 */

@interface DeathViewController : UIViewController

@end
