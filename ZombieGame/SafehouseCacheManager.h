//
//  SafehouseCacheManager.h
//  ZombieGame
//
//  Created by TheAppExperts on 3/13/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

/*!
* @brief Protocol for determining how the app behaves when safehouses successfully or unsuccessfully download.
*/

@protocol SafehouseDelegate <NSObject>

@required
/*!
 * @brief Method called when safehouses successfully download.
 * @param safehouses An array containing the found safehouses.
 */
- (void)safehousesFound:(NSArray *)safehouses;

/*!
 * @brief Method called when safehouses fail to download
 */
- (void)safehousesNotFound;
@end

/*!
 * @brief Manager maintaining the download of safehouse data via JSON.
 */
@interface SafehouseCacheManager : NSObject

/*!
 * @brief The delegate that runs the safehouse protocol methods.
 */
@property (strong, nonatomic) UIViewController <SafehouseDelegate> *delegate;

/*!
 * @brief Gets the safehouses from a local cached file.
 */
- (void) getSafehousesFromLocalFile;

/*!
 * @brief Attempts to get the safehouses from the internet. If it fails, it runs getSafehousesFromLocalFile as a backup instead.
 */
- (void) getSafehousesFromTheNet;

@end
