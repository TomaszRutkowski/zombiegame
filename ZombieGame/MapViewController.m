//
//  MapViewController.m
//  ZombieGame
//
//  Created by TheAppExperts on 3/13/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//


#import "MapViewController.h"

@interface MapViewController ()
{
    enum mapState {stateNOTLOADED, statePARTIAL, stateFULL, stateOUT};
    enum mapState mapRendered;
}
@end

@implementation MapViewController

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    if ([UIApplication sharedApplication].keyWindow.traitCollection.forceTouchCapability == UIForceTouchCapabilityAvailable)
    {
        if ([touches anyObject].force > 2.0)
        {
            if (![self.selected isEqual: nil] && ![self.selected isEqualToString: @"My Location"])
            {
                UITapGestureRecognizer *sender = [[UITapGestureRecognizer alloc] init];
                [self performSegueWithIdentifier:@"MapToSafehouse" sender:sender];
            }
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([sender isKindOfClass:[UITapGestureRecognizer class]])
    {
        for (MKPointAnnotation *point in self.mapView.annotations)
        {
            for (NSDictionary *safehouse in self.arrayOfSafehouses)
            {
                if (point.title == [safehouse valueForKey:@"Name"])
                {
                    ((SafehouseViewController *)segue.destinationViewController).safehouse = safehouse;
                }
            }
        }
    }
}

- (IBAction)userTapped:(id)sender
{
    NSLog(@"User Tapped");
    if (![self.selected isEqual: nil] && ![self.selected isEqualToString: @"My Location"])
    {
        [self performSegueWithIdentifier:@"MapToSafehouse" sender:sender];
    }
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    NSLog(@"Selected: %@", view.annotation.title);
    self.selected = view.annotation.title;
}

- (void)safehousesFound:(NSArray *)safehouses
{
    NSLog(@"Safehouses Found!");
    for (NSDictionary *safehouse in safehouses)
    {
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        double lat = [[safehouse valueForKey:@"Lat"] doubleValue];
        NSLog(@"%f",lat);
        double longt = [[safehouse valueForKey:@"Long"] doubleValue];
        NSLog(@"%f",longt);
        point.coordinate = CLLocationCoordinate2DMake(lat, longt);
        point.title = [safehouse valueForKey:@"Name"];
        point.subtitle = @"Safehouse";
        [self.mapView addAnnotation:point];
    }
    self.arrayOfSafehouses = safehouses;
}

- (void)safehousesNotFound
{
    UIAlertController *alert = [[UIAlertController alloc] init];
    [alert setTitle:@"Failed to load safehouses"];
    [alert setMessage:@"The safehouses failed to load."];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){[self dismissViewControllerAnimated:YES completion:^(void){}];}];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:^(void){}];
}

- (BOOL)randomEvent
{
    NSLog(@"Fired Random Event");
    [[LoggedInData sharedLoggedInData].LoggedInAccount.stats walkingDrainWithNight:FALSE];
    if (arc4random_uniform(10) == 0) //90% chance of success
    {
        return FALSE;
    }
    else
    {
        switch (arc4random_uniform(5)) {
            case 0:
            case 1:
            case 2:
                [self performSegueWithIdentifier:@"MapToEvent" sender:self];
            break;
            case 3:
            case 4:
                [self performSegueWithIdentifier:@"MapToCombat" sender:self];
            break;
            default:
            return FALSE; //error
            break; //if we reach here something majorly wrong is happening
        }
        return TRUE;
    }
}

- (void)mapViewDidFinishRenderingMap:(MKMapView *)mapView fullyRendered:(BOOL)fullyRendered
{
    //this code could be improved, but works for now
    if (fullyRendered == TRUE)
    {
        NSLog(@"Called Fully Rendered");
        
        if (mapRendered == stateNOTLOADED)
        {
            mapRendered = statePARTIAL; //it's getting ready, but we don't want it to fire too soon
        }
        else
        {
            if (mapRendered == statePARTIAL)
            {
                if ([self.loading isHidden] == FALSE)
                {
                    [self.loading hideAnimated:YES];
                }
                mapRendered = stateFULL;
            }
        }
    }
}

- (void)mapViewDidFailLoadingMap:(MKMapView *)mapView withError:(NSError *)error
{
    UIAlertController *alert = [[UIAlertController alloc] init];
    [alert setTitle:@"Failed to load map"];
    [alert setMessage:@"The map failed to load, please ensure Apple maps are correctly configured."];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){[self dismissViewControllerAnimated:YES completion:^(void){}];}];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:^(void){}];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    NSLog(@"Called Location");
    MKCoordinateRegion mapRegion;
    mapRegion.center = [locations lastObject].coordinate;
    mapRegion.span = MKCoordinateSpanMake(0.01, 0.01);
    if (mapRegion.center.longitude == -180.00000000)
    {
        NSLog(@"Invalid region, temporarily ignoring!");
    }
    else
    {
        [self.mapView setRegion:mapRegion animated: NO];
    }
    if (mapRendered == stateFULL)
    {
        [self randomEvent];
    }
    if (mapRendered == statePARTIAL)
    {
        if ([self.loading isHidden] == FALSE)
        {
            [self.loading hideAnimated:YES];
        }
        mapRendered = stateFULL;
    }
}


- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if ([CLLocationManager locationServicesEnabled])
    {
        if (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusAuthorizedAlways)
        {
            [manager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
            [manager setDistanceFilter:100];
            [self.mapView setShowsUserLocation:YES];
            [manager startUpdatingLocation];
        }
        else
        {
            [manager stopUpdatingLocation];
        }
    }
    else
    {
        [manager stopUpdatingLocation];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"Appear");
    if (mapRendered == statePARTIAL || mapRendered == stateOUT)
    {
        if ([self.loading isHidden] == FALSE)
        {
            [self.loading hideAnimated:YES];
        }
        mapRendered = stateFULL;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    NSLog(@"Disappear");
    if (mapRendered == stateFULL)
    {
        mapRendered = stateOUT;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //get safehouses to store
    self.selected = nil;
    self.safehouseManager = [[SafehouseCacheManager alloc] init];
    [self.safehouseManager setDelegate:self];
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"multiplayer"] boolValue] == TRUE)
    {
        [self.safehouseManager getSafehousesFromTheNet];
    }
    else
    {
        [self.safehouseManager getSafehousesFromLocalFile];
    }
    
    //do map stuff
    self.loading = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.loading setMode: MBProgressHUDModeIndeterminate];
    [self.loading.label setText:@"Loading"];
    mapRendered = stateNOTLOADED;
    self.mapView.delegate = self;
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self.mapView setScrollEnabled:FALSE];
    [self.mapView setZoomEnabled:FALSE];
    [self.mapView setRotateEnabled:FALSE];
    
    if ([CLLocationManager locationServicesEnabled])
    {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
        {
            UIAlertController *alert = [[UIAlertController alloc] init];
            [alert setTitle:@"Location Services are not authorised"];
            [alert setMessage:@"You must authorise location services to play this game."];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){[self dismissViewControllerAnimated:YES completion:^(void){}];}];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:^(void){}];
        }
        else
        {
            if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined)
            {
                [self.locationManager requestWhenInUseAuthorization];
            }
            if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse)
            {
                [self.locationManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
                [self.locationManager setDistanceFilter:100];
                [self.mapView setShowsUserLocation:YES];
                [self.locationManager startUpdatingLocation];
            }
        }
    }
    else
    {
        UIAlertController *alert = [[UIAlertController alloc] init];
        [alert setTitle:@"Location Services are not enabled"];
        [alert setMessage:@"You must enable location services to play this game."];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){[self dismissViewControllerAnimated:YES completion:^(void){}];}];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:^(void){}];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
