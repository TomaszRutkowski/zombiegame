//
//  EventViewController.h
//  ZombieGame
//
//  Created by TheAppExperts on 3/14/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoggedInData.h"
#import "Item+ItemMethods.h"

/**
* @brief The view that displays a non-combat event. Currently, it only does item drops.
*/

@interface EventViewController : UIViewController

//pretty self explanatory
@property (strong, nonatomic) IBOutlet UIImageView *itemImage;
@property (strong, nonatomic) IBOutlet UILabel *foundLabel;
@property (strong, nonatomic) IBOutlet UIButton *goOutButton;

/**
 * @brief Haptic feedback for when the screen appears.
 */

@property (strong, nonatomic) UINotificationFeedbackGenerator *feedback;

- (IBAction)leaveScreen:(id)sender;

/**
 * @brief Does the event to drop a random item.
 */

- (void)addRandomItem;

@end
