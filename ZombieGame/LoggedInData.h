//
//  LoggedInData.h
//  ZombieGame
//
//  Created by TheAppExperts on 3/9/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Account+AccountMethods.h"

@interface LoggedInData : NSObject

@property (strong, nonatomic) Account *LoggedInAccount;

@property (nonatomic) bool onlineMode;
@property (nonatomic) bool colorblindMode;
@property (nonatomic) NSNumber *masterVolume;

+ (instancetype) sharedLoggedInData;

- (void)fetchSettings;

@end
