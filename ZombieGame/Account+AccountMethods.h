//
//  Account+AccountMethods.h
//  ZombieGame
//
//  Created by TheAppExperts on 3/9/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import "Account+CoreDataClass.h"
#import "Account+CoreDataProperties.h"
#import "CoreDataManager.h"

@interface Account (AccountMethods)

+ (BOOL) validName:(NSString *)name;
+ (BOOL) validPassword:(NSString *)password;
+ (BOOL) validEmail:(NSString *)email;

+(Account *)createAccount:(NSString *) name withPass:(NSString *) password withEmail:(NSString *) email withContext:(NSManagedObjectContext *) context;
+(Account *)loginAccount:(NSString *)name withPass:(NSString *)password withContext:(NSManagedObjectContext *) context;

@end
