//
//  LoggedInData.m
//  ZombieGame
//
//  Created by TheAppExperts on 3/9/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import "LoggedInData.h"

@implementation LoggedInData

+ (instancetype) sharedLoggedInData
{
    static LoggedInData *sharedLoggedInData;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedLoggedInData = [[self alloc] init];
    });
    return sharedLoggedInData;
}

- (void)fetchSettings
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults synchronize];
    
    if ([defaults valueForKey:@"multiplayer"])
    {
        self.onlineMode = [defaults boolForKey:@"multiplayer"];
    }
    else
    {
        self.onlineMode = TRUE;
    }
    
    if ([defaults valueForKey:@"colorblind"])
    {
        self.colorblindMode= [defaults boolForKey:@"colorblind"];
    }
    else
    {
        self.colorblindMode = FALSE;
    }
}

@end
