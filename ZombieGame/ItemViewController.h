//
//  ItemViewController.h
//  ZombieGame
//
//  Created by TheAppExperts on 3/10/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Item+ItemMethods.h"
#import "LoggedInData.h"

/**
 * @brief The view controller that shows items and allows you to use them.
 */

@interface ItemViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *itemImageView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *typeLabel;
@property (strong, nonatomic) IBOutlet UILabel *quantityLabel;
@property (strong, nonatomic) IBOutlet UILabel *ammoLabel;
@property (strong, nonatomic) IBOutlet UIButton *useButton;
@property (strong, nonatomic) IBOutlet UIButton *goBackButton;

@property (strong, nonatomic) Item *lookedAtItem;

- (IBAction)useAction:(id)sender;
- (IBAction)goBackAction:(id)sender;

@end
