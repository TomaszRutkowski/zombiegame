//
//  SafehouseCacheManager.m
//  ZombieGame
//
//  Created by TheAppExperts on 3/13/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import "SafehouseCacheManager.h"

@implementation SafehouseCacheManager

- (void)getSafehousesFromLocalFile
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
    ^{
        NSString *fileName = [[NSBundle mainBundle] pathForResource:@"safehouse" ofType:@"json"];
        if (fileName)
        {
            NSData *safehouseData = [[NSData alloc] initWithContentsOfFile:fileName];
            NSError *error;
            NSDictionary *safehouseRoot = [NSJSONSerialization JSONObjectWithData:safehouseData options:0 error:&error];
    
            if (error) {
                NSLog(@"Error with JSON: %@", error.localizedDescription);
                [self.delegate safehousesNotFound];
            }
            else
            {
                NSLog(@"Safehouses Found On Local File");
                [self.delegate safehousesFound:(NSArray *)[safehouseRoot objectForKey:@"safehouses"]];
            }
        }
        else
        {
            [self.delegate safehousesNotFound];
        }
    });
}

- (void)getSafehousesFromTheNet
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
    ^{
        NSError *error;
        NSString *urlString = @"https://pastebin.com/raw/iWk0mEub";
        NSData *safehouseData = [NSData dataWithContentsOfURL: [NSURL URLWithString:urlString]];
        NSDictionary *safehouseRoot = [NSJSONSerialization JSONObjectWithData:safehouseData options:kNilOptions error:&error];
        if (error)
        {
            NSLog(@"Error with JSON: %@", error.localizedDescription);
            [self getSafehousesFromLocalFile];
        }
        else
        {
            NSLog(@"Safehouses Found On Net");
            [self.delegate safehousesFound:(NSArray *)[safehouseRoot objectForKey:@"safehouses"]];
        }
    });
}

@end
