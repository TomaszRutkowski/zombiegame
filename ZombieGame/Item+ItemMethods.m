//
//  Item+ItemMethods.m
//  ZombieGame
//
//  Created by TheAppExperts on 3/10/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import "Item+ItemMethods.h"

@implementation Item (ItemMethods)

+ (Item *)StarterItemWithContext:(NSManagedObjectContext *)context
{
    Item *newItem = [[Item alloc] initWithEntity:[NSEntityDescription entityForName:@"Item" inManagedObjectContext:context] insertIntoManagedObjectContext:context];
    newItem.name = @"Ration";
    newItem.type = @"Food";
    newItem.qty = (short)5;
    newItem.ammo = (short)0;
    return newItem;
}

+ (Item *)RandomItemWithContext:(NSManagedObjectContext *)context
{
    Item *newItem = [[Item alloc] initWithEntity:[NSEntityDescription entityForName:@"Item" inManagedObjectContext:context] insertIntoManagedObjectContext:context];
    switch (arc4random_uniform(4)) {
        case 0:
        {
            newItem.name = @"Health Kit";
            newItem.type = @"Healing";
            newItem.qty = (short)arc4random_uniform(2) + 1;
            newItem.ammo = (short)0;
            return newItem;
        }
        break;
        case 1:
        {
            newItem.name = @"Sandwich";
            newItem.type = @"Food";
            newItem.qty = (short)arc4random_uniform(3) + 1;
            newItem.ammo = (short)0;
            return newItem;
        }
        break;
        case 2:
        {
            newItem.name = @"Water Bottle";
            newItem.type = @"Water";
            newItem.qty = (short)arc4random_uniform(4) + 1;
            newItem.ammo = (short)0;
            return newItem;
        }
        break;
        case 3:
        {
            newItem.name = @"Adrenaline";
            newItem.type = @"Drug";
            newItem.qty = (short)arc4random_uniform(2) + 1;
            newItem.ammo = (short)0;
            return newItem;
        }
        break;
        default:
        {
            newItem.name = @"Ration";
            newItem.type = @"Food";
            newItem.qty = (short)5;
            newItem.ammo = (short)0;
            return newItem;
        }
        break;
    }
}

- (void)useItemInAccount:(Account *)account
{
    NSError *error;
    for (Item *item in account.items) {
        if (item.name == self.name)
        {
            if (item.qty > 1)
            {
                item.qty = item.qty - 1;
                [account.stats changeStatswithItemType:item.type inAccount:account];
                [account.managedObjectContext save:&error]; //no problem if it doesn't work, stats won't get saved either!
            }
            else
            {
                NSMutableOrderedSet *newItems = [account.items mutableCopy];
                [newItems removeObject:item];
                account.items = newItems;
                [account.stats changeStatswithItemType:item.type inAccount:account];
                [account.managedObjectContext save:&error]; //no problem if it doesn't work, stats won't get saved either!
            }
        }
    }
}

- (UIImage *)imageForItem
{
    if ([self.name isEqualToString: @"Sandwich"])
    {
        return [UIImage imageNamed:@"sandwich.png"];
    }
    if ([self.name isEqualToString: @"Water Bottle"])
    {
        return [UIImage imageNamed:@"water.png"];
    }
    if ([self.name isEqualToString: @"Health Kit"])
    {
        return [UIImage imageNamed:@"health.png"];
    }
    if ([self.name isEqualToString: @"Adrenaline"])
    {
        return [UIImage imageNamed:@"adrenaline.png"];
    }
    if ([self.name isEqualToString: @"Ration"])
    {
        return [UIImage imageNamed:@"sandwich.png"];
    }
    return nil;
}

@end
