//
//  MailDelegate.h
//  ZombieGame
//
//  Created by TheAppExperts on 3/12/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>

/**
 * @brief Protocol for notifying a screen that the user has left the email screen.
 */

@protocol MailFinished <NSObject>

@required
- (void) mailFinished:(BOOL)Sent;
@end

/**
 * @brief Class for handling email related events.
 */

@interface MailDelegate : NSObject <MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) UIViewController<MailFinished> *delegate;

@end
