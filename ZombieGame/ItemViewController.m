//
//  ItemViewController.m
//  ZombieGame
//
//  Created by TheAppExperts on 3/10/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import "ItemViewController.h"

@interface ItemViewController ()

@end

@implementation ItemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.nameLabel setText: [NSString stringWithFormat:@"Name:  %@", self.lookedAtItem.name]];
    [self.typeLabel setText: [NSString stringWithFormat:@"Type:  %@", self.lookedAtItem.type]];
    [self.quantityLabel setText: [NSString stringWithFormat:@"Quantity:  %i", (int)self.lookedAtItem.qty]];
    if ([self.lookedAtItem.type isEqualToString: @"Gun"])
    {
        [self.ammoLabel setText: [NSString stringWithFormat:@"Ammo:  %i", (int)self.lookedAtItem.ammo]];
    }
    else
    {
        [self.ammoLabel setHidden:TRUE];
    }
    [self.itemImageView setImage: self.lookedAtItem.imageForItem];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)useAction:(id)sender
{
    [self.lookedAtItem useItemInAccount:[LoggedInData sharedLoggedInData].LoggedInAccount];
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (IBAction)goBackAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

@end
