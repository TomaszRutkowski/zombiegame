//
//  AppDelegate.h
//  ZombieGame
//
//  Created by TheAppExperts on 3/6/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) EKEventStore *eventStore;
@property (strong, nonatomic) EKCalendar *calendar;

@end

