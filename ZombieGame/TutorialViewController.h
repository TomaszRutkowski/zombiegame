//
//  TutorialViewController.h
//  ZombieGame
//
//  Created by TheAppExperts on 3/12/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YTPlayerView.h"

/**
 * @brief View controller that plays a youtube video for the tutorial.
 */

@interface TutorialViewController : UIViewController <YTPlayerViewDelegate>

@property (strong, nonatomic) IBOutlet YTPlayerView *youtubePlayer;

@end
