//
//  ItemTableDelegate.h
//  ZombieGame
//
//  Created by TheAppExperts on 3/10/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "LoggedInData.h"
#import "Item+ItemMethods.h"

@protocol ItemTableDelegateProtocol <NSObject>

@required
- (void)SegueWithItem:(Item *)item;

@end

/**
 * @brief Seperate class for any item table (although not used for safehouse).
 */

@interface ItemTableDelegate : NSObject <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) UIViewController <ItemTableDelegateProtocol> *delegate;

@end
