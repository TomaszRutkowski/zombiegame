//
//  MailDelegate.m
//  ZombieGame
//
//  Created by TheAppExperts on 3/12/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import "MailDelegate.h"

@implementation MailDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    BOOL didSend;
    
    switch (result) {
        case MFMailComposeResultCancelled:
        {
            didSend = FALSE;
                [controller dismissViewControllerAnimated:YES completion:^(void){}];
            break;
        }
        case MFMailComposeResultSaved:
        {
            didSend = FALSE;
                [controller dismissViewControllerAnimated:YES completion:^(void){}];
            break;
        }
        case MFMailComposeResultSent:
        {
            didSend = TRUE;
                [controller dismissViewControllerAnimated:YES completion:^(void){}];
            break;
        }
        case MFMailComposeResultFailed:
        {
            didSend = FALSE;
                [controller dismissViewControllerAnimated:YES completion:^(void){}];
            break;
        }
            
        default:
            break;
    }
    [controller dismissViewControllerAnimated:NO completion:^(void){[self.delegate mailFinished:didSend];}];
}

@end
