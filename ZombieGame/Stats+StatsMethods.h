//
//  Stats+StatsMethods.h
//  ZombieGame
//
//  Created by TheAppExperts on 3/9/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import "Account+AccountMethods.h"
#import "Stats+CoreDataClass.h"
#import "Stats+CoreDataProperties.h"

@interface Stats (StatsMethods)

+ (Stats *) initStatsWithContext:(NSManagedObjectContext*)context;
- (void) walkingDrainWithNight:(BOOL)night;
- (void) changeStatswithItemType:(NSString *)type inAccount:(Account *)account;

@end
