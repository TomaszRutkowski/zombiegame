//
//  SafehouseViewController.m
//  ZombieGame
//
//  Created by TheAppExperts on 3/16/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import "SafehouseViewController.h"

@interface SafehouseViewController ()

@end

@implementation SafehouseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@", [self.safehouse valueForKey:@"Name"]);
    [self.inventoryTableView setDelegate:self];
    [self.inventoryTableView setDataSource:self];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString: @"SafehouseToPictureView"])
    {
        ((PictureViewController *)segue.destinationViewController).safehouseIn = [self.safehouse valueForKey:@"Name"];
    }
}

- (IBAction)leaveAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^(void){}];
}

- (IBAction)restAction:(id)sender {
    [LoggedInData sharedLoggedInData].LoggedInAccount.stats.tiredness = 100;
    NSError *error;
    [[LoggedInData sharedLoggedInData].LoggedInAccount.managedObjectContext save:&error]; //doesn't matter if this fails
}

- (IBAction)pictureViewAction:(id)sender {
    [self performSegueWithIdentifier:@"SafehouseToPictureView" sender:self];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section)
    {
        case 0:
        {
            return [[self.safehouse objectForKey:@"Items"] count];
        }
        break;
        case 1:
        {
            return [[LoggedInData sharedLoggedInData].LoggedInAccount.items count];
        }
        break;
        default:
        {
            return 0;
        }
        break;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section)
    {
        case 0:
        {
            return @"Safehouse Items";
        }
        break;
        case 1:
        {
            return @"Your Inventory";
        }
        break;
        default:
        {
            return @"Error";
        }
        break;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *newCell = [tableView dequeueReusableCellWithIdentifier:@"SafehouseCell"];
    if (newCell == nil)
    {
       newCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"SafehouseCell"];
    }
    
    [newCell setBackgroundColor:[UIColor colorWithRed:0.4 green:0.6 blue:0.1 alpha:0.0]];
    
    switch (indexPath.section)
    {
        case 0:
        {
            [newCell.textLabel setText:[[[self.safehouse objectForKey:@"Items"] objectAtIndex:indexPath.row] valueForKey:@"Name"]];
            [newCell.detailTextLabel setText: [(NSNumber *)[[[self.safehouse objectForKey:@"Items"] objectAtIndex:indexPath.row] valueForKey:@"Qty"] stringValue]];
        }
        break;
        case 1:
        {
            [newCell.textLabel setText:[[[LoggedInData sharedLoggedInData].LoggedInAccount.items objectAtIndex:indexPath.row] valueForKey:@"Name"]];
            [newCell.detailTextLabel setText:[(NSNumber *)[[[LoggedInData sharedLoggedInData].LoggedInAccount.items objectAtIndex:indexPath.row] valueForKey:@"Qty"] stringValue]];
        }
        break;
        default:
        {
            [newCell.textLabel setText:@"Error"];
        }
        break;
    }

    return newCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section)
    {
    case 0:
    {

    }
    break;
    case 1:
    {

    }
    break;
    default:
    {
        //nothing
    }
    break;
    }
}

@end
