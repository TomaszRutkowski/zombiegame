//
//  SafehouseViewController.h
//  ZombieGame
//
//  Created by TheAppExperts on 3/16/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoggedInData.h"
#import "Stats+StatsMethods.h"
#import "Item+ItemMethods.h"
#import "ZombieGame-Swift.h"

/*!
 * @brief SafehouseViewController is a view controller that renders the internal
 * view of a safehouse.
 * @Warning This is not a Table View to allow flexibility in the future.
 */

@interface SafehouseViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

/*!
 * @brief The safehouse being looked at in dictionary form.
 */
@property (strong, nonatomic) NSDictionary *safehouse;

//These are self explanatory.
@property (strong, nonatomic) IBOutlet UIButton *leaveButton;
@property (strong, nonatomic) IBOutlet UIButton *restButton;
@property (strong, nonatomic) IBOutlet UIButton *pictureButton;

/*!
 * @brief The table view containing both the safehouse and player inventory. The safehouse inventory is in section 0, while the player inventory is in section 1.
 */
@property (strong, nonatomic) IBOutlet UITableView *inventoryTableView;

/*!
 * @brief The action when the user leaves the safehouse.
 * @param sender The UI element triggering the action.
 */
- (IBAction)leaveAction:(id)sender;

/*!
 * @brief The action when the user uses the rest action, which restores tiredness.
 * @param sender The UI element triggering the action.
 */
- (IBAction)restAction:(id)sender;

/*!
 * @brief The action when the user wants to view a picture of the safehouse in the real world.
 * @param sender The UI element triggering the action.
 */
- (IBAction)pictureViewAction:(id)sender;

@end
