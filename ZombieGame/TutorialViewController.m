//
//  TutorialViewController.m
//  ZombieGame
//
//  Created by TheAppExperts on 3/12/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import "TutorialViewController.h"

@interface TutorialViewController ()

@end

@implementation TutorialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.youtubePlayer loadWithVideoId:@"uPFwKdVe3J8"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
