//
//  CombatViewController.h
//  ZombieGame
//
//  Created by TheAppExperts on 3/14/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoggedInData.h"
#import "Stats+StatsMethods.h"

/**
* @brief The view that displays during a combat event.
*/

@interface CombatViewController : UIViewController

//self explanatory

@property (strong, nonatomic) IBOutlet UIImageView *zombieImage;
@property (strong, nonatomic) IBOutlet UILabel *playerHealthLabel;
@property (strong, nonatomic) IBOutlet UILabel *enemyHealthLabel;
@property (strong, nonatomic) IBOutlet UIProgressView *playerHealthBar;
@property (strong, nonatomic) IBOutlet UIProgressView *enemyHealthBar;
@property (strong, nonatomic) IBOutlet UILabel *dangerLabel;
@property (nonatomic) double enemyHealth;

/**
 * @brief The max health of the enemy (set to full when combat starts).
 */
@property (nonatomic) double enemyHealthMax;

/**
 * @brief Haptic feedback for when strikes happen.
 */

@property (strong, nonatomic) UINotificationFeedbackGenerator *feedback;

- (IBAction)userSwiped:(id)sender;

@end
