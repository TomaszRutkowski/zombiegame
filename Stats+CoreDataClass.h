//
//  Stats+CoreDataClass.h
//  ZombieGame
//
//  Created by TheAppExperts on 3/10/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Account;

NS_ASSUME_NONNULL_BEGIN

@interface Stats : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Stats+CoreDataProperties.h"
