//
//  Stats+CoreDataProperties.m
//  ZombieGame
//
//  Created by TheAppExperts on 3/10/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import "Stats+CoreDataProperties.h"

@implementation Stats (CoreDataProperties)

+ (NSFetchRequest<Stats *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Stats"];
}

@dynamic health;
@dynamic hunger;
@dynamic thirst;
@dynamic tiredness;
@dynamic account;

@end
