//
//  ZombieGameTests.m
//  ZombieGameTests
//
//  Created by TheAppExperts on 3/6/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <XCTest/XCTest.h>
#import "Account+AccountMethods.h"
#import "SafehouseCacheManager.h"
#import "LoggedInData.h"
#import "MapViewController.h"

@interface ZombieGameTests : XCTestCase

@end

@implementation ZombieGameTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testValidName{
    NSArray *testArray = @[@"Sunder", @"temmy123", @"FLaX2"];
    
    for(NSString *string in testArray)
    {
        XCTAssertTrue([Account validName:string]);
    }
}

- (void)testInvalidName{
    NSArray *testArray = @[@"S@m", @"f_g_d_23", @"23."];
    for(NSString *string in testArray)
    {
        XCTAssertFalse([Account validName:string]);
    }
}

- (void)testValidPass{
    NSArray *testArray = @[@"1a5bCtD2A", @"Abc123", @"banana"];
    
    for(NSString *string in testArray)
    {
        XCTAssertTrue([Account validName:string]);
    }
}

- (void)testInvalidPass{
    NSArray *testArray = @[@"gitG$d", @"f_g_d_23", @">_>"];
    for(NSString *string in testArray)
    {
        XCTAssertFalse([Account validName:string]);
    }
}

- (void)testLocalFile{
    NSArray *safehouseArray = [[SafehouseCacheManager sharedSafehouseCacheManager] getSafehousesFromLocalFile];
    XCTAssertNotNil(safehouseArray);
    XCTAssertNotEqual(safehouseArray, @[]);
    NSLog(@"%@", [[safehouseArray objectAtIndex:0] valueForKey:@"Name"]);
}

- (void)testDefaultSettings{
  /*  [[LoggedInData sharedLoggedInData] fetchSettings];
    XCTAssertEqual([LoggedInData sharedLoggedInData].colorblindMode, FALSE);
    XCTAssertEqual([LoggedInData sharedLoggedInData].onlineMode, TRUE); */
}

- (void)testRandomEvent{
    MapViewController *mvc = [[MapViewController alloc] init];
    int i;
    BOOL found;
    for (i=0; i <20; i++)
    {
        if ([mvc randomEvent] == TRUE)
        {
            found = TRUE;
        }
    }
    XCTAssertTrue(found);
}


@end
