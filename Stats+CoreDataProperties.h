//
//  Stats+CoreDataProperties.h
//  ZombieGame
//
//  Created by TheAppExperts on 3/10/17.
//  Copyright © 2017 TheAppExperts. All rights reserved.
//

#import "Stats+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Stats (CoreDataProperties)

+ (NSFetchRequest<Stats *> *)fetchRequest;

@property (nonatomic) double health;
@property (nonatomic) double hunger;
@property (nonatomic) double thirst;
@property (nonatomic) double tiredness;
@property (nullable, nonatomic, retain) Account *account;

@end

NS_ASSUME_NONNULL_END
